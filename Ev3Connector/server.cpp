#include "server.hpp"

#include <memory>
#include <utility>

using namespace std::chrono_literals;

#ifdef TCP_ENABLED

Server::Server (std::string host, unsigned short port)
	: _host (std::move (host)), _port (port), _running (false)
{ }

#else

Server::Server (std::string sockPath)
	: _sockPath (std::move (sockPath)), _running (false)
{ }

#endif

Server::~Server ()
{
	stop ();
}

void Server::sendPacket (Packet& packet)
{
	std::lock_guard<std::mutex> lock (_sendMutex);
	_sendQueue.push (packet);
}

Packet Server::getReceivedPacket ()
{
	std::lock_guard<std::mutex> lock (_receiveMutex);
	Packet tmp = _receiveQueue.front ();
	_receiveQueue.pop ();
	return tmp;
}

Event Server::waitForEvent (std::chrono::milliseconds timeToWait)
{
	unsigned int i = 0;

	while (i < timeToWait.count ()) {
		{
			std::lock_guard<std::mutex> lock (_receiveMutex);
			if (!_receiveQueue.empty ()) {
				return PacketReceived;
			}
			else if (!_running) {
				return Disconnected;
			}
		}
		i += 10;
		std::this_thread::sleep_for (10ms);
	}

	return NoEvent;
}

void Server::stop ()
{
	_running = false;

	if (_thread != nullptr && _thread->joinable ()) {
		_thread->join ();
	}
}

void Server::internalLoop ()
{
#ifdef TCP_ENABLED
	Socket s (_host, _port);
#else
	Socket s (_sockPath);
#endif

	while (_running) {
		if (s.canRead (10ms)) {
			auto rep = s.read ();
			if (rep.first) {
				_running = false;
			}
			else {
				parsePackets (rep.second);
			}
		}
		{
			std::lock_guard<std::mutex> lock (_sendMutex);
			if (!_sendQueue.empty ()) {
				if (s.write ("\n" + to_string (_sendQueue.front ()) + "\n")) {
					_running = false;
				}
				_sendQueue.pop ();
			}
		}

	}

	s.close ();
}

void Server::parsePackets (const std::string& buffer)
{
	long beginPos = static_cast<long >(buffer.find ('\n', 0));
	long endPos = static_cast<long >(buffer.find ('\n', beginPos + 1));

	while (beginPos != std::string::npos && endPos != std::string::npos) {
		{
			std::lock_guard<std::mutex> lockGuard (_receiveMutex);
			_receiveQueue
				.push (Packet::parse (buffer.begin () + beginPos, buffer.begin () + endPos));
		}

		beginPos = static_cast<long >(buffer.find ('\n', endPos + 1));
		endPos = static_cast<long >(buffer.find ('\n', beginPos + 1));
	}

}

void Server::start ()
{
	_running = true;
	_thread = std::make_unique<std::thread> (&Server::internalLoop, std::ref (*this));
}
