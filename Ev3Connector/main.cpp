#include <iostream>
#include <csignal>
#include "server.hpp"

using namespace std::chrono_literals;

bool running = true;

void signal_handler (int signal)
{
	std::cout << "Signal Caught." << std::endl;
	running = false;
}

int main (int argc, char *argv[])
{
	signal (SIGINT, signal_handler);
	signal (SIGTERM, signal_handler);

#ifdef TCP_ENABLED
	if (argc < 3) {
		std::cout << "Usage : " << std::endl << argv[0] << " <host> <port>" << std::endl;
		return 0;
	}
	Server _s (argv[1], atoi (argv[2]));
	std::cout << "Using " << argv[1] << "@" << atoi (argv[2]) << " as TCP Socket." << std::endl;
#else
	if (argc < 2) {
		std::cout << "Usage : " << std::endl << argv[0] << " <unix socket path>" << std::endl;
		return 0;
	}
	Server _s (argv[1]);
	std::cout << "Using " << argv[1] << " as Unix Socket." << std::endl;
#endif

	std::cout << "Starting Server..." << std::endl;
	_s.start ();
	Packet p = {{"msg", "Ready"}};
	_s.sendPacket (p);

	while (running) {
		switch (_s.waitForEvent (1s)) {
			case NoEvent:
				break;

			case Disconnected:
				std::cout << "Disconnected." << std::endl;
				running = false;
				break;

			case PacketReceived:
				Packet pp = _s.getReceivedPacket ();
				std::cout << "Packet Received : " << pp.dump () << std::endl;
				_s.sendPacket (pp);
				break;
		}
	}
	std::cout << "Stopping Server..." << std::endl;
	_s.stop ();
	std::cout << "End." << std::endl;
	return 0;
}