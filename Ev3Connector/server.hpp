#ifndef _SERVER_HPP_
#define _SERVER_HPP_

#include <thread>
#include <queue>
#include <mutex>
#include <atomic>

#ifdef TCP_ENABLED
#include "tcp-socket.hpp"

#else
#include "unix-socket.hpp"
#endif

#include "json.hpp"

using Packet = nlohmann::json;

enum Event {
	NoEvent,
	Disconnected,
	PacketReceived,
};

class Server {
public:
#ifdef TCP_ENABLED
	explicit Server (std::string host, unsigned short port);
#else
	explicit Server (std::string sockPath);
#endif
	~Server ();

	Event waitForEvent (std::chrono::milliseconds timeToWait);

	Packet getReceivedPacket ();

	void sendPacket (Packet& packet);

	void stop ();
	void start ();
private:
	void internalLoop ();
	void parsePackets (const std::string& buffer);

	std::queue<Packet> _receiveQueue;
	std::queue<Packet> _sendQueue;

	std::unique_ptr<std::thread> _thread = nullptr;

	std::mutex _receiveMutex;
	std::mutex _sendMutex;

	std::atomic_bool _running;

#ifdef TCP_ENABLED
	std::string _host;
	unsigned short _port;
#else
	std::string _sockPath;
#endif

};

#endif //_SERVER_HPP_
