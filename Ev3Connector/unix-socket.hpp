#ifndef BASIC_SERVER_SOCKET_H
#define BASIC_SERVER_SOCKET_H

#include <memory>
#include <chrono>
#include <atomic>
#include <mutex>

class Socket {
public:
	explicit Socket (const std::string& path);
	~Socket ();

	Socket (Socket&) = delete;
	Socket& operator= (const Socket&) = delete;
	Socket (Socket&&) = delete;
	Socket& operator= (Socket&&) = delete;

	bool canRead (std::chrono::milliseconds timeout) const;

	std::pair<bool, std::string> read () const;
	bool write (const std::string& data) const;

	bool isOpen ();
	void close ();

private:
	std::unique_ptr<std::string> _buf;

	[[noreturn]] static void raiseError (const std::string& msg);
	static void sendWarning (const std::string& msg);

	unsigned short _poll (unsigned short event, int timeout_ms) const;
	std::pair<bool, size_t>
	_write (const std::string& data, size_t offset) const;

	int _s;
	std::atomic_bool _open;
	std::mutex _m;
};

#endif // BASIC_SERVER_SOCKET_H
