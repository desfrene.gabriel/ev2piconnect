#include <sys/socket.h>
#include <sys/poll.h>
#include <unistd.h>
#include <iostream>
#include <cstring>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netdb.h>
#include <fcntl.h>

#include "tcp-socket.hpp"

constexpr size_t BUFFER_SIZE = 256;
constexpr size_t MAX_ATTEMPTS = 5;

Socket::Socket (const std::string& host, unsigned short port)
	: _buf (new std::string (BUFFER_SIZE, 0x00)), _open (false)
{
	struct addrinfo hints = {0};
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = 0;
	hints.ai_protocol = IPPROTO_TCP;

	struct addrinfo *result = nullptr;

	int status = getaddrinfo (host.c_str (), std::to_string (port).c_str (), &hints, &result);
	if (status == EAI_SYSTEM) {
		Socket::raiseError ("Unable to find remote host address");
	}
	else if (status != 0) {
		errno = 0;
		Socket::raiseError (std::string (gai_strerror (status)));
	}

	for (struct addrinfo *rp = result; rp != nullptr; rp = rp->ai_next) {
		_s = socket (rp->ai_family, rp->ai_socktype, rp->ai_protocol);

		if (_s > 0) {
			// Socket created
			if (connect (_s, rp->ai_addr, rp->ai_addrlen) != -1) {
				// Socket connected
				break;
			}
			else {
				// Socket not connected
				::close (_s);
				_s = 0;
			}
		}
		else {
			// Socket not created
			_s = 0;
		}
	}

	freeaddrinfo (result);

	if (_s < 0) {
		Socket::raiseError ("Unable to create a socket");
	}

	_open = true;
}

Socket::~Socket ()
{
	close ();
}

unsigned short Socket::_poll (unsigned short event, int timeout_ms) const
{
	pollfd pollEvents = {0};
	pollEvents.fd = _s;
	pollEvents.events = static_cast<short>(event);

	size_t status = ::poll (&pollEvents, 1, timeout_ms);

	if (status == static_cast<size_t>(-1)) {
		Socket::raiseError ("Unable to poll socket");
	}
	else {
		return static_cast<unsigned short>(pollEvents.revents);
	}
}

std::pair<bool, std::string> Socket::read () const
{
	if (_s == -1) {
		Socket::sendWarning ("Unable to read on closed Socket");
		return std::make_pair (true, std::string ());
	}

	long status = ::recv (_s, &(*_buf)[0], BUFFER_SIZE, 0);

	if (status == -1) {
		if (errno == EWOULDBLOCK || errno == ECONNREFUSED ||
		    errno == ENOTCONN || errno == EINTR || errno == ECONNRESET) {
			Socket::sendWarning ("Error during 'read()'");
			return std::make_pair (true, std::string ());
		}
		else {
			Socket::raiseError ("Error during 'read()'");
		}
	}
	else if (status != 0) {
		return std::make_pair (false, std::string (_buf->begin (), _buf->begin () + status));
	}
	else {
		Socket::sendWarning ("EOF received");
		return std::make_pair (true, std::string ());
	}
}

bool Socket::write (const std::string& data) const
{
	if (_s == -1) {
		Socket::sendWarning ("Unable to write on closed Socket");
		return true;
	}

	size_t nbSent = 0;
	unsigned char nbTries = 0;

	while (nbSent < data.size () && nbTries < MAX_ATTEMPTS) {
		auto status = _poll (POLLOUT, 10);
		if (status & POLLOUT) {
			auto rep = _write (data, nbSent);

			if (rep.first) {
				return true;
			}
			else {
				nbSent += rep.second;
				nbTries++;
			}
		}
		else if (status & (POLLNVAL | POLLHUP | POLLERR)) {
			Socket::sendWarning ("Unable to send data through socket, connection seems closed");
			return true;
		}
	}

	if (nbTries == MAX_ATTEMPTS) {
		Socket::sendWarning (
			"Unable to send data after " + std::to_string (MAX_ATTEMPTS) + " attempts");
		return true;
	}
	else {
		return false;
	}
}

std::pair<bool, size_t>
Socket::_write (const std::string& data, size_t offset) const
{
	size_t status = send (_s, &data[offset], data.size () - offset, MSG_NOSIGNAL);

	if (status == static_cast<size_t>(-1)) {
		if (errno == EWOULDBLOCK || errno == ECONNRESET ||
		    errno == EDESTADDRREQ || errno == ENOTCONN ||
		    errno == EPIPE || errno == EINTR) {
			sendWarning ("Error during '_write()'");
			return std::make_pair (true, 0);
		}
		else {
			Socket::raiseError ("Error during '_write()'");
		}
	}
	else {
		return std::make_pair (false, status);
	}
}

[[noreturn]] void Socket::raiseError (const std::string& msg)
{
	std::cerr << "[SOCKET ERROR] : " << msg << '.' << std::endl;

	if (errno != 0) {
		std::cerr << "Error number " << errno << " : " << std::endl
		          << std::strerror (errno) << std::endl;
	}

	exit (errno);
}

void Socket::sendWarning (const std::string& msg)
{
	std::cout << "[SOCKET WARNING] : " << msg << ", stopping actual session." << std::endl;

	if (errno != 0) {
		std::cout << "Error number " << errno << " : " << std::endl
		          << std::strerror (errno) << std::endl;
	}
}

bool Socket::canRead (std::chrono::milliseconds timeout) const
{
	return _open && (_poll (POLLIN, static_cast<int>(timeout.count ())) & POLLIN);
}

bool Socket::isOpen ()
{
	return _open;
}

void Socket::close ()
{
	std::lock_guard<std::mutex> lock (_m);
	if (isOpen ()) {
		_open = false;

		if (::close (_s) < 0) {
			sendWarning ("Unable to close Socket");
		}
		_s = -1;
	}
}

