#!/usr/bin/env bash
remote_sock_path=$(curl "http://ev3.desfrene.fr/socket")
local_sock_path="$(pwd)/test.sock"
control_ssh_socket="$(pwd)/ssh.sock"
srv_path="/home/gabriel/Desktop/Ev2PiConnect/Ev3Connector/cmake-build-debug/Test"

echo "Using remote_sock_path : $remote_sock_path"
echo "Using local_sock_path : $local_sock_path"

ssh -M -S "$control_ssh_socket" -fNT -L "$local_sock_path:$remote_sock_path" ev3connector@desfrene.fr
ssh_status=$(ssh -S "$control_ssh_socket" -O check ev3connector@desfrene.fr)

echo "SSH status : $ssh_status"
echo "Start Server"
$srv_path "$local_sock_path"

echo "Stop SSH..."
ssh_status=$(ssh -S "$control_ssh_socket" -O exit ev3connector@desfrene.fr)
echo "SSH status : $ssh_status"

rm "$local_sock_path"