import socket

import json
import logging

import selectors
import threading
import queue

import atexit

BUFFER_SIZE = 256
NEW_LINE = '\n'


class TCPServer(object):
    _sock_port: int = 5959

    def __init__(self, send_queue: queue.Queue, recv_queue: queue.Queue):
        atexit.register(self.stop)

        logging.info("Starting UNIX Server...")

        self._send_q = send_queue
        self._recv_q = recv_queue

        self._e = threading.Event()
        self._connected = False

        self._sel = selectors.DefaultSelector()

        self._server_thread = threading.Thread(target=self._server_loop)
        self._server_thread.daemon = True

        logging.debug(f"Starting Server Thread")
        self._server_thread.start()

    def stop(self):
        logging.info(f"Stopping UNIX Server")
        self._e.set()

        self._server_thread.join()
        logging.debug(f"Server stopped")

    def _server_loop(self):
        logging.debug(f"Initializing UNIX Socket...")
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM, socket.IPPROTO_TCP)

        s.bind(('', self._sock_port))
        logging.debug(f"Socket bound to '{self._sock_port}'")

        s.listen(1)
        s.setblocking(False)

        logging.debug("Registering Server Callback")
        self._sel.register(s, selectors.EVENT_READ, self._client_loop)

        logging.debug("Server Socket Loop started")
        while not self._e.is_set():
            events = self._sel.select(0.01)  # 0.01s
            for key, mask in events:
                callback = key.data
                callback(key.fileobj)

        logging.debug(f"Event Thread set. Closing Server Socket...")

        s.close()
        logging.debug(f"Server Socket closed.")

    def _client_loop(self, sock: socket.socket):
        client, address = sock.accept()
        logging.info(f"New client connected : '{address}'. Starting new Session...")

        self._connected = True

        logging.debug("Registering Client Callback...")
        self._sel.register(client,
                           selectors.EVENT_READ | selectors.EVENT_WRITE,
                           (self._read, self._write))

        logging.debug("Started Client Loop")
        while not self._e.is_set() and self._connected:
            events = self._sel.select(0.01)  # 0.01s
            for key, mask in events:
                if mask & selectors.EVENT_READ:
                    callback, _ = key.data
                    callback(key.fileobj)
                elif mask & selectors.EVENT_WRITE:
                    _, callback = key.data
                    callback(key.fileobj)

        logging.debug("Stopped Client Loop. Closing Client Socket...")
        client.close()

        self._connected = False
        logging.info(f"Client '{address}' Session closed.")

    def _read(self, client: socket.socket):
        buf = client.recv(BUFFER_SIZE)
        if buf:
            logging.debug(f"New Packet Received : '{buf.decode().replace(NEW_LINE, '')}'")
            self._recv_q.put(json.loads(buf))
        else:
            logging.debug(f"Client Disconnected. Stopping Client Loop...")
            self._sel.unregister(client)
            self._connected = False

    def _write(self, client: socket.socket):
        if not self._send_q.empty():
            packet = json.dumps(self._send_q.get())
            client.sendall(('\n' + packet + '\n').encode())
            logging.debug(f"Sent Packet : '{packet}'")

    def send_packet(self, json_packet: dict):
        self._send_q.put(json.dumps(json_packet))

    def recv_packet(self) -> dict:
        return self._recv_q.get()

    def has_packet(self) -> bool:
        return not self._recv_q.empty()

    @property
    def socket(self) -> int:
        return self._sock_port

    @property
    def connected(self) -> bool:
        return self._connected
