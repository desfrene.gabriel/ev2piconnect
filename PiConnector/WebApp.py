import json
import logging
import queue

from UnixServer import UnixServer
from TCPServer import TCPServer


class Application(object):
    def __init__(self, tcp=False):
        self._recv_q = queue.Queue()
        self._send_q = queue.Queue()

        if tcp:
            self._srv = TCPServer(self._recv_q, self._send_q)
        else:
            self._srv = UnixServer(self._recv_q, self._send_q)

    def __call__(self, environ: dict, start_response):
        path = environ["PATH_INFO"]

        if path == "/get":
            code, data = self.get_packet(environ)
        elif path == "/put":
            code, data = self.put_packet(environ)
        elif path == "/socket":
            code, data = self.get_sock_path(environ)
        elif path == "/connected":
            code, data = self.get_connected(environ)
        else:
            code, data = self.error(environ)

        start_response(code, [
            ("Content-Type", "text/json"),
            ("Content-Length", str(len(data)))
        ])
        return iter([data])

    def put_packet(self, environ: dict) -> (str, bytes):
        if environ['REQUEST_METHOD'] != "POST" or 'CONTENT_LENGTH' not in environ:
            return self.error(environ)

        data_length = int(environ['CONTENT_LENGTH'])
        recv_packet = json.loads(environ['wsgi.input'].read(data_length))
        self._srv.send_packet(recv_packet)

        return "200 OK", b"OK"

    def get_packet(self, environ: dict) -> (str, bytes):
        if environ['REQUEST_METHOD'] != "GET":
            return self.error(environ)

        if self._srv.has_packet():
            return "200 OK", json.dumps(self._srv.recv_packet()).encode()
        else:
            return "200 OK", b"NO PACKET"

    def get_sock_path(self, environ: dict) -> (str, bytes):
        if environ['REQUEST_METHOD'] != "GET":
            return self.error(environ)

        return "200 OK", f"{self._srv.socket}".encode()

    @staticmethod
    def error(environ: dict) -> (str, bytes):
        return "500 Internal Server Error", f"Unable to proceed to {environ['REQUEST_METHOD']} " \
                                            f"request '{environ['PATH_INFO']}'".encode()

    def get_connected(self, environ: dict) -> (str, bytes):
        if environ['REQUEST_METHOD'] != "GET":
            return self.error(environ)

        return "200 OK", f"{self._srv.connected}".encode()
