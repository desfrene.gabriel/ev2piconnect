import logging
from WebApp import Application

logging.basicConfig(format="[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s",
                    level=logging.DEBUG, datefmt='%Y-%m-%d %H:%M:%S %z')

app = Application(tcp=False)
